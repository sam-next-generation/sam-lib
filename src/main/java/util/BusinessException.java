/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author rachmanareef
 */
public class BusinessException extends Exception{
    private String rc;

    public BusinessException(String message) {
        super(message);
        this.rc = "1";
    }

    public BusinessException(String rc, String message) {
        super(message);
        this.rc = rc;
    }

    public String getRc() {
        return rc;
    }

    public void setRc(String rc) {
        this.rc = rc;
    }
}
