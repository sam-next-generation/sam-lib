/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.syn.sam.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author rachmanareef
 */
@Entity
@Table(name = "ref_bicheck")
public class RefBicheck implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "bi_check")
    private Short biCheck;
    @Column(name = "bi_def")
    private String biDef;

    public RefBicheck() {
    }

    public RefBicheck(Short biCheck) {
        this.biCheck = biCheck;
    }

    public Short getBiCheck() {
        return biCheck;
    }

    public void setBiCheck(Short biCheck) {
        this.biCheck = biCheck;
    }

    public String getBiDef() {
        return biDef;
    }

    public void setBiDef(String biDef) {
        this.biDef = biDef;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (biCheck != null ? biCheck.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RefBicheck)) {
            return false;
        }
        RefBicheck other = (RefBicheck) object;
        if ((this.biCheck == null && other.biCheck != null) || (this.biCheck != null && !this.biCheck.equals(other.biCheck))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.syn.sam.model.RefBicheck[ biCheck=" + biCheck + " ]";
    }
    
}
